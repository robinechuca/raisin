#!/usr/bin/env python3

"""
** Serialization of default objects. **
---------------------------------------
"""


import dill

from raisin.serialization.constants import HEADER


def serialize_default(obj, compact):
    """
    ** Use ``dill`` to serialize cases not supported by raisin. **
    """
    yield HEADER['default'][compact]
    yield dill.dumps(obj, recurse=True)

def deserialize_default(pack, gen, *_):
    r"""
    ** Deserialize dict. **

    Examples
    --------
    >>> from raisin.serialization.atoms.default import serialize_default, deserialize_default
    >>> func = lambda x: x**2
    >>> ser_func = b''.join(serialize_default(func, True))
    >>> func_bis = deserialize_default(ser_func[1:], [])
    >>> [func_bis(i) for i in range(3)]
    [0, 1, 4]
    >>>
    """
    return dill.loads(pack + b''.join(gen))
