#!/usr/bin/env python3

"""
** Allows for extensive testing around communication. **
--------------------------------------------------------
"""


import multiprocessing
import queue
import socket
import threading


from raisin.communication.connection import QueueConnection
from raisin.communication.connection_socket import SocketConnection



def check_simple_send_recv(conn1, conn2):
    """
    ** Performs simple tests on the 2 listening connections. **
    """
    conn1.send('hello')
    assert conn2.recv() == ({}, 'hello')
    conn1.send(0)
    conn1.send(1)
    assert conn2.recv() == ({}, 0)
    assert conn2.recv() == ({}, 1)

def check_order_send_recv(conn1, conn2):
    """
    ** Checks the order of data reception. **
    """
    conn1.send(metadata={'num': 0})
    conn1.send(metadata={'num': 1})
    conn1.send(metadata={'num': 2})
    conn1.send(metadata={'num': 3})
    conn1.send(metadata={'num': 4})
    assert conn2.recv({'num': 0}) == ({'num': 0}, None)
    assert conn2.recv({'num': 2}) == ({'num': 2}, None)
    assert conn2.recv({'num': 4}) == ({'num': 4}, None)
    assert conn2.recv({'num': 1}) == ({'num': 1}, None)
    assert conn2.recv({'num': 3}) == ({'num': 3}, None)

def check_family_send_recv(conn1, conn2):
    """
    ** Check that we can recover a family of messages. **
    """
    conn1.send(metadata={'a': True})
    conn1.send(metadata={'a': False})
    conn1.send(metadata={'b': True})
    conn1.send(metadata={'b': False})
    conn1.send(metadata={'a': True, 'b': True})
    conn1.send(metadata={'a': True, 'b': False})
    conn1.send(metadata={'a': False, 'b': True})
    conn1.send(metadata={'a': False, 'b': False})

    assert conn2.recv({'a': True}) == ({'a': True}, None)
    assert conn2.recv({'a': True}) == ({'a': True, 'b': True}, None)
    assert conn2.recv({'a': True}) == ({'a': True, 'b': False}, None)
    assert conn2.recv({'b': True}) == ({'b': True}, None)
    assert conn2.recv({'b': True}) == ({'a': False, 'b': True}, None)
    assert conn2.recv({'a': False, 'b': False}) == ({'a': False, 'b': False}, None)
    assert conn2.recv({'a': False}) == ({'a': False}, None)
    assert conn2.recv({'b': False}) == ({'b': False}, None)

def check_thread_safe(conn1, conn2):
    """
    ** Send messages at the same time to ensure there are no conflicts. **
    """
    def send(conn, min_val, nbr):
        for val in range(min_val, min_val+nbr):
            conn.send(metadata={'val': val})
    nbr_p_threads = 20
    nbr_threads = 10
    threads = [
        threading.Thread(target=send, args=(conn1, min_val, nbr_p_threads))
        for min_val in range(0, nbr_threads*nbr_p_threads, nbr_p_threads)
    ]
    for thread in threads:
        thread.start()
    for val in range(nbr_threads*nbr_p_threads):
        assert conn2.recv({'val': val}) == ({'val': val}, None)
    for thread in threads:
        thread.join()

def start_test_close(conn1, conn2):
    """
    ** remove duplication of code lines. **
    """
    conn1.start()
    conn2.start()
    check_simple_send_recv(conn1, conn2)
    check_order_send_recv(conn1, conn2)
    check_family_send_recv(conn1, conn2)
    check_thread_safe(conn1, conn2)
    conn1.close()
    conn2.close()

def test_thread_queue():
    """
    ** Test communications passing through single queues. **
    """
    queue_12, queue_21 = queue.Queue(), queue.Queue()
    conn1, conn2 = QueueConnection(queue_12, queue_21), QueueConnection(queue_21, queue_12)
    start_test_close(conn1, conn2)

def test_multiprocessing_queue():
    """
    ** Test communications passing through single queues. **
    """
    queue_12, queue_21 = multiprocessing.SimpleQueue(), multiprocessing.SimpleQueue()
    conn1, conn2 = QueueConnection(queue_12, queue_21), QueueConnection(queue_21, queue_12)
    start_test_close(conn1, conn2)

def test_socket():
    """
    ** Tests communication over a pair of TCP connections. **
    """
    socket1, socket2 = socket.socketpair()
    conn1, conn2 = SocketConnection(socket1), SocketConnection(socket2)
    start_test_close(conn1, conn2)
