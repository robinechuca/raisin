#!/usr/bin/env python3

"""
** Specialization of a connection pair between 2 sockets. **
------------------------------------------------------------
"""


import itertools
import socket

from raisin.communication.connection import Connection
from raisin.serialization.iter_tools import anticipate, size2tag, tag2size



class SocketConnection(Connection):
    """
    ** A connection between 2 pairs of sockets. **

    Examples
    --------
    >>> import socket
    >>> from raisin.communication.connection_socket import SocketConnection
    >>> socket1, socket2 = socket.socketpair()
    >>> conn1, conn2 = SocketConnection(socket1), SocketConnection(socket2)
    >>> conn1.start()
    >>> conn2.start()
    >>> conn1.send('hello')
    >>> conn2.recv()
    ({}, 'hello')
    >>> conn1.close()
    >>> conn2.close()
    >>>
    """

    def __init__(self, sock):
        """
        Parameters
        ----------
        sock : socket.socket
            A socket capable of reading and sending data.
        """
        super().__init__()
        assert isinstance(sock, socket.socket), sock.__class__.__name__
        self.sock = sock
        self._recv_pack = b'' # buffer memory

    def send_data(self, raw):
        """
        Implements ``raisin.communication.connection.Connection.send_data``.
        """
        for is_end, pack in anticipate(raw):
            signed_pack = bytes([is_end]) + size2tag(len(pack)) + pack
            try:
                self.sock.sendall(signed_pack)
            except OSError as err:
                raise ConnectionError('communication seems to be interrupted') from err

    def recv_data(self):
        """
        Implements ``raisin.communication.connection.Connection.recv_data``.
        """

        def get_pack():
            """ get the following package """
            try:
                data = self.sock.recv(4096)
            except socket.timeout as err:
                raise TimeoutError(
                    'the reception is interrupted because the time over'
                ) from err
            except ConnectionResetError as err:
                raise err
            except OSError as err:
                raise ConnectionError('communication seems to be interrupted') from err
            else:
                if not data: # socket closed
                    raise ConnectionResetError(
                        'data received are empty, the socket is probably closed')
                return data

        while True:
            # check for len and last pack
            if not self._recv_pack:
                self._recv_pack += get_pack()
            is_end, self._recv_pack = bool(self._recv_pack[0]), self._recv_pack[1:]
            size, self._recv_pack, _ = tag2size(
                pack=self._recv_pack,
                gen=(get_pack() for _ in itertools.count(0))
            )
            while len(self._recv_pack) < size:
                self._recv_pack += get_pack()
            yield self._recv_pack[:size]
            self._recv_pack = self._recv_pack[size:]
            if is_end:
                break

    def close(self):
        """
        ** Use a more efficient way to permanently close the connection. **
        """
        try:
            self.sock.shutdown(socket.SHUT_RDWR)
        except OSError:
            pass
        super().close()
