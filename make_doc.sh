#!/bin/bash

./run_tests.sh
if [ $? -ne 0 ]; then
    echo "Avant de generer la doc, il faut que tous les tests passent."
    exit 1
else
    clear
fi

# graph
appl=$(find raisin/application -name "*.py")
comm=$(find raisin/communication -name "*.py")
enca=$(find raisin/encapsulation -name "*.py")
seri=$(echo raisin/serialization/*.py)
pyreverse --all-ancestors -f OTHER --output dot raisin/__init__.py raisin/__main__.py $appl $comm $enca $seri
if [ $? -ne 0 ]; then
    exit 1
fi
fdp -Lg -LO -Ln100 -LU3 -LC100 -Tpng classes.dot -o classes.png
dot -Tpng packages.dot -o packages.png
rm *.dot

# doc html
pdoc3 raisin/ -c latex_math=True --force --html
if [ $? -ne 0 ]; then
    exit 1
fi

# deplacement
rm -r ~/serveur_doc_python/raisin/*
mv html/raisin/* ~/serveur_doc_python/raisin/
rm -r html
cp classes.png ~/serveur_doc_python/raisin/classes.png
cp packages.png ~/serveur_doc_python/raisin/packages.png

exit 0
